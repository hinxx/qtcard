QtCard
======

Updated: 28 Mar 2021


QtCard is a simple memory smart card manipulation utility. It was designed
to facilitate the initialization, tracking and updating of the SLE4442
memory cards. Memory cards were configured to be used in a electronic wallet
system.

QtCard is meant to be used with the ACS ACR30U USB smart card reader. ACR30U
can be controlled using low-level USB access library (libusb-0.1). See Wiki
for more info.

Features
--------

* read, verify and update SLE4442 memory smart cards

* tested on Windows 10 and Linux OS

* latest release does not use Sqlite DB anymore



License
=======

See LICENSE.


Build
=====

Get Qt4 4.8.7 from https://download.qt.io/archive/qt/4.8/4.8.7/qt-opensource-windows-x86-mingw482-4.8.7.exe archive and start installation.
It will prompt for MinGW-64 bit version and offer a link for download - use it.

This is an example mingw-64 installer:

https://kent.dl.sourceforge.net/project/mingw-w64/Toolchains%20targetting%20Win32/Personal%20Builds/mingw-builds/4.8.2/threads-posix/dwarf/i686-4.8.2-release-posix-dwarf-rt_v3-rev3.7z

Get 7z (https://www.7-zip.org/download.html) and unpack the mingw-64 archive to C:\, to create C:\mingw32 folder.

Finish both installations, Qt4 will be in C:\Qt\4.8.7, mingw32 in C:\mingw32.

Install qtcreator 2.5 from https://download.qt.io/archive/qtcreator/2.5/
https://download.qt.io/archive/qtcreator/2.5/qt-creator-win-opensource-2.5.2.exe

Finish installation, QtCreator will be in C:\Qt\qtcreator-2.5.2.

Create Toolkit and toolchain ; must use 32-bit build, even on 64-bit machines!

Disable shadow builds in QtCreator, otherwise the libusb-0.1.12 folder is not found.

Deploy
======

Use Release build in QtCreator.
Use http://www.dependencywalker.com/ to find out which DLLs the executable needs.
See deploy.zip for example (tested on Windows 10 in March 2021).


Links
=====

*    Linux USB library
 http://libusb.sourceforge.net

*    Window USB library
 http://sourceforge.net/apps/trac/libusb-win32/wiki

*    Icons
 http://www.iconki.com/smartcard-icons-174-p2.asp

*    ACS ACR30U PCSC-lite bundle
 David Corcoran <corcoran@linuxnet.com>

*    PCSC-lite
 http://pcsclite.alioth.debian.org/pcsclite.html



Contact
=======

Hinko Kocevar <hinkocevar@gmail.com>

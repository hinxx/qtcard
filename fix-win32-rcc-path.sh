#!/bin/sh

# Run this script to fix the RCC.exe path in the Makefile.Release and
# Makefile.Debug, which is not set correctly (Qt for windows bug).

# Set path to Qt install path
QTPATH="c:/Qt/4.8.3"

# Add Qt path to the rcc.exe call
sed -i -e "s%\tbin/rcc.exe%\t$QTPATH/bin/rcc.exe%" \
	Makefile.Release Makefile.Debug

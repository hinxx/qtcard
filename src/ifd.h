/*
 * ifd.h
 *
 *  Created on: Nov 21, 2012
 *      Author: hinkocevar@gmail.com
 *     Project: QtCard
 *     Copying: See LICENSE
 *    Web page: https://bitbucket.org/hinxx/qtcard
 */

#ifndef IFD_H_
#define IFD_H_

#include <QtDebug>
#include <QObject>
#include <QMap>

#include "acr30u.h"

#define _UNUSED(arg) (void)arg;
#define DBG_INFO		__FILE__ << __func__ << __LINE__ << ": "

// List of defines available to ifd
#define IFD_POWER_UP				500
#define IFD_POWER_DOWN				501
#define IFD_RESET					502

#define IFD_NEGOTIATE_PTS1			1
#define IFD_NEGOTIATE_PTS2			2
#define IFD_NEGOTIATE_PTS3			4

#define	IFD_SUCCESS					0
#define IFD_ERROR_TAG				600
#define IFD_ERROR_SET_FAILURE		601
#define IFD_ERROR_VALUE_READ_ONLY	602
#define IFD_ERROR_PTS_FAILURE		605
#define IFD_ERROR_NOT_SUPPORTED		606
#define IFD_PROTOCOL_NOT_SUPPORTED	607
#define IFD_ERROR_POWER_ACTION		608
#define IFD_ERROR_SWALLOW			609
#define IFD_ERROR_EJECT				610
#define IFD_ERROR_CONFISCATE		611
#define IFD_COMMUNICATION_ERROR		612
#define IFD_RESPONSE_TIMEOUT		613
#define IFD_NOT_SUPPORTED			614
#define IFD_ICC_PRESENT				615
#define IFD_ICC_NOT_PRESENT			616


class ifd {
public:
	ifd();
	~ifd();

	// List of function as supported by the PCSC IFD_Handler V3
	// Note the compatibility with PCSC is not a case here, only that
	// IFD function set seems appropriate to use here.
	bool create_channel(unsigned long a_lun, const char *a_device_name);
	bool destroy_channel(unsigned long a_lun);
	bool get_capabilities(unsigned long a_lun,
			unsigned long a_tag,
			unsigned long *a_length,
			unsigned char *a_value);
	bool set_capabilities(unsigned long a_lun,
			unsigned long a_tag,
			unsigned long a_length,
			unsigned char *a_value);
	bool set_protocol_parameters(unsigned long a_lun,
			unsigned long a_protocol,
			unsigned char a_flags,
			unsigned char a_PTS1,
			unsigned char a_PTS2,
			unsigned char a_PTS3);
	bool power_ICC(unsigned long a_lun,
			unsigned long a_action,
			unsigned char *a_atr,
			unsigned long *a_atr_length);
	bool transmit_to_ICC(unsigned long a_lun,
			unsigned long a_proto,
			unsigned char *a_tx_buffer,
			unsigned long a_tx_length,
			unsigned char *a_rx_buffer,
			unsigned long *a_rx_length);
	bool control(unsigned long a_lun,
			unsigned long a_control_code,
			unsigned char *a_tx_buffer,
			unsigned long a_tx_length,
			unsigned char *a_rx_buffer,
			unsigned long *a_rx_length);
	bool is_IFD_present(unsigned long a_lun);
	bool is_ICC_present(unsigned long a_lun);

private:
	QMap<unsigned long, acr30u *> m_acr30u;

};


#endif /* IFD_H_ */

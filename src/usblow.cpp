/*
 * usblow.cpp
 *
 *  Created on: Nov 21, 2012
 *      Author: hinkocevar@gmail.com
 *     Project: QtCard
 *     Copying: See LICENSE
 *    Web page: https://bitbucket.org/hinxx/qtcard
 */

#include "usblow.h"

#ifdef USBLOW_DEBUG
inline QDebug usbdbg() { return QDebug(QtDebugMsg); }
#else
inline QNoDebug usbdbg() { return QNoDebug(); }
#endif

usblow::usblow(unsigned short a_vend_id, unsigned short a_prod_id,
		unsigned long a_ep_in, unsigned long a_ep_out) {
	usbdbg() << DBG_INFO;

	m_usbdev = NULL;
	m_vend_id = a_vend_id;
	m_prod_id = a_prod_id;
	m_ep_in = a_ep_in;
	m_ep_out = a_ep_out;
	m_config = 1;
	m_intf = 0;

	// initialize usb
	usb_init();
	// set debugging level
	// XXX: set to 0 once stable
	//usb_set_debug(255);
	usb_set_debug(0);
}

usblow::~usblow() {
	usbdbg() << DBG_INFO;

	close_dev();
}

bool usblow::open_dev() {
	usbdbg() << DBG_INFO;

	struct usb_bus *bus;
	struct usb_device *dev;

	if (m_usbdev) {
		usbdbg() << DBG_INFO << "usb device already opened!";
		return true;
	}

	usb_find_busses();
	usb_find_devices();

	for (bus = usb_get_busses(); bus; bus = bus->next) {
		for (dev = bus->devices; dev; dev = dev->next) {
			if (dev->descriptor.idVendor == m_vend_id &&
				dev->descriptor.idProduct == m_prod_id) {
				usbdbg() << DBG_INFO << "found ACR30U VID "
						<< QString("%1").arg(dev->descriptor.idVendor, 0, 16)
						<< " PID "
						<< QString("%1").arg(dev->descriptor.idProduct, 0, 16);

				// get device handle
				usb_dev_handle *usb_dev = usb_open(dev);
				if (! usb_dev) {
					usbdbg() << DBG_INFO << "usb_open() failed: "
							<< usb_strerror();
					return false;
				}

				// set configuration and claim the interface now!
				usbdbg() << DBG_INFO << "setting configuration #" << m_config;
				if (usb_set_configuration(usb_dev, m_config) < 0) {
					usbdbg() << DBG_INFO << "usb_set_configuration() failed: "
							<< usb_strerror();
					usb_close(usb_dev);
					return false;
				}
				usbdbg() << DBG_INFO << "set configuration #" << m_config;

				usbdbg() << DBG_INFO << "claiming interface #" << m_intf;
				if (usb_claim_interface(usb_dev, m_intf) < 0) {
					usbdbg() << DBG_INFO << "usb_claim_interface() failed: "
							<< usb_strerror();
					usb_close(usb_dev);
					return false;
				}
				usbdbg() << DBG_INFO << "claimed interface #" << m_intf;

				// store the USB device handle
				m_usbdev = usb_dev;
				return true;
			}
		}
	}

	usbdbg() << DBG_INFO << "failed to find our USB device";
	return false;
}

bool usblow::close_dev() {
	usbdbg() << DBG_INFO;

	if (! m_usbdev) {
		usbdbg() << DBG_INFO << "usb device not opened!";
		return false;
	}

	usb_release_interface(m_usbdev, m_intf);
	usb_close(m_usbdev);
	m_usbdev = NULL;

	return true;
}

bool usblow::write_dev(unsigned long length, unsigned char *buffer) {
	usbdbg() << DBG_INFO;

	long rv = -1;
	unsigned long request_type;

	if (! m_usbdev) {
		usbdbg() << DBG_INFO << "usb device not opened!";
		return false;
	}

	QStringList sl;
	for (unsigned long i = 0; i < length; ++i) {
		sl.append(QString("%1").arg(buffer[i], 0, 16));
	}
	usbdbg() << DBG_INFO << "length: " << length
			<< " data: " << sl.join(" ");

	// vendor specific request type for device
	request_type = USB_RECIP_DEVICE | USB_TYPE_VENDOR | m_ep_out;

	// make a bulk output request
	rv = usb_control_msg(
		m_usbdev,		// usb device handle
		request_type,	// request type
		0, 				// request
		0,				// value
		0,				// index
		(char *)buffer,	// data
		length,			// data length
		100000);		// timeout

	if (rv < 0) {
		usbdbg() << DBG_INFO << "usb_control_msg() failed: "
				<< usb_strerror();
		return false;
	}

	return true;
}

bool usblow::read_dev(unsigned long *length, unsigned char *buffer) {
	usbdbg() << DBG_INFO;

	long rv = -1;
	long len = *length;

	if (! m_usbdev) {
		usbdbg() << DBG_INFO << "usb device not opened!";
		return false;
	}

	// make a bulk input request
	rv = usb_bulk_read(
		m_usbdev,		// usb device handle
		m_ep_in,		// bulk in endpoint
		(char *)buffer,	// data
		len,			// data length
		50000);			// timeout

	if (rv < 0) {
		usbdbg() << DBG_INFO << "usb_bulk_read() failed: "
				<< usb_strerror();
		return false;
	}

	// actual data length read from the device
	*length = rv;

	QStringList sl;
	for (unsigned long i = 0; i < *length; ++i) {
		sl.append(QString("%1").arg(buffer[i], 0, 16));
	}
	usbdbg() << DBG_INFO << "length: " << *length
			<< " data: " << sl.join(" ");

	return true;
}


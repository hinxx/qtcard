/*
 * acr30u.h
 *
 *  Created on: Nov 21, 2012
 *      Author: hinkocevar@gmail.com
 *     Project: QtCard
 *     Copying: See LICENSE
 *    Web page: https://bitbucket.org/hinxx/qtcard
 */

#ifndef ACR30U_H_
#define ACR30U_H_

#include <QtDebug>
#include <QObject>

#include "usblow.h"

#define _UNUSED(arg) (void)arg;
#define DBG_INFO		__FILE__ << __func__ << __LINE__ << ": "

#define ACR30U_MAX_BUFFER_SIZE		264
#define ACR30U_MAX_ATR_SIZE			33

#define ACR30U_NOTIFY_TRUE			0x01
#define ACR30U_NOTIFY_FALSE			0x02

// ACR30U card type codes
#define ACR30U_CARDTYPE_AUTO		0x00	// auto select for T=0 and T=1
#define ACR30U_CARDTYPE_GPM103		0x01	// GPM103
#define ACR30U_CARDTYPE_SLE44X6		0x01	// SLE4406, SLE4436, SLE5536
#define ACR30U_CARDTYPE_I2C			0x02	// I2C type cards
#define ACR30U_CARDTYPE_44X8		0x05	// SLE4418, SLE4428
#define ACR30U_CARDTYPE_44X2		0x06	// SLE4432, SLE4442
#define ACR30U_CARDTYPE_T0			0x0C	// MCU T=0
#define ACR30U_CARDTYPE_T1			0x0D	// MCU T=1
#define ACR30U_CARDTYPE_SAMT0		0xC0	// SAM T=0 protocol
#define ACR30U_CARDTYPE_SAMT1		0xD0	// SAM T=1 protocol

// SLE44x2 specific codes
#define ACR30U_SLE44X2_RESET		0x80
#define ACR30U_SLE44X2_PWROFF		0x81
#define ACR30U_SLE44X2_MEMRD		0x90
#define ACR30U_SLE44X2_MEMWR		0x91
#define ACR30U_SLE44X2_PROTWR		0x94
#define ACR30U_SLE44X2_PRESENT		0x92
#define ACR30U_SLE44X2_CHANGE		0x93

// IFD control codes
#define ACR30U_IFD_RAW				0x00
#define ACR30U_IFD_RESET			0xA0
#define ACR30U_IFD_POWER			0xA1
#define ACR30U_IFD_UNPOWER			0xA2
#define ACR30U_IFD_NOTIFY			0xA3
#define ACR30U_IFD_SETCARD			0xA4

// CT codes (MCT 0.9 specification)
#define ACR30U_ICC_SEL_FILE			0xA4
#define ACR30U_ICC_READ_BIN			0xB0
#define ACR30U_ICC_UPDATE_BIN		0xD6
#define ACR30U_ICC_VERIFY			0x20
#define ACR30U_ICC_CHANGE			0x24

class acr30u {
public:
	acr30u(unsigned long a_lun);
	~acr30u(void);

	bool initialize(void);
	void deinitialize(void);
	bool is_attached(void);
	bool get_stats(unsigned char *a_status);
	bool is_card_present(void);
	bool power_card(unsigned char *a_atr, unsigned long *a_atr_length);
	bool unpower_card(void);
	bool reset_card(unsigned char *a_atr, unsigned long *a_atr_length);
	bool select_card(unsigned char a_card_type);
	bool set_notification(unsigned char a_state);
	bool transmit(unsigned char *a_tx_buffer,
			unsigned long a_tx_length,
			unsigned char *a_rx_buffer,
			unsigned long *a_rx_length);
	bool raw_exchange(unsigned char *a_tx_buffer,
			unsigned long a_tx_length,
			unsigned char *a_rx_buffer,
			unsigned long *a_rx_length);
private:

	void convert2ascii(unsigned char *a_bin_buffer,
			unsigned long a_bin_length,
			unsigned char *a_ascii_buffer,
			unsigned long *a_ascii_length);
	void convert2hex (unsigned char *a_ascii_buffer,
			unsigned long a_ascii_length,
			unsigned char *a_bin_buffer,
			unsigned long *a_bin_length);
	unsigned char checksum(unsigned char *a_buffer, unsigned long a_length);

	usblow *m_usb;
	unsigned long m_lun;
};


#endif /* ACR30U_H_ */

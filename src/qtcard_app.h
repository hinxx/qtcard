/*
 * qtcard_app.h
 *
 *  Created on: Nov 21, 2012
 *      Author: hinkocevar@gmail.com
 *     Project: QtCard
 *     Copying: See LICENSE
 *    Web page: https://bitbucket.org/hinxx/qtcard
 */

#ifndef QTCARD_APP_H_
#define QTCARD_APP_H_

#include <QApplication>

class qtcard_app: public QApplication {
public:
	qtcard_app(int &argc, char **argv);
	~qtcard_app();
};


#endif /* QTCARD_APP_H_ */

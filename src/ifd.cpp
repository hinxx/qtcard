/*
 * ifd.cpp
 *
 *  Created on: Nov 21, 2012
 *      Author: hinkocevar@gmail.com
 *     Project: QtCard
 *     Copying: See LICENSE
 *    Web page: https://bitbucket.org/hinxx/qtcard
 */


#include "ifd.h"

#ifdef IFD_DEBUG
inline QDebug ifddbg() { return QDebug(QtDebugMsg); }
#else
inline QNoDebug ifddbg() { return QNoDebug(); }
#endif

ifd::ifd() {
	ifddbg() << DBG_INFO;

}

ifd::~ifd() {
	ifddbg() << DBG_INFO;

	// destroy all acr30u objects
	QMapIterator<unsigned long, acr30u *> it(m_acr30u);
	while (it.hasNext()) {
		it.next();
		destroy_channel(it.key());
	}
}

bool ifd::create_channel(unsigned long a_lun, const char *a_device_name) {
	ifddbg() << DBG_INFO << "device" << a_device_name << "LUN" << a_lun;
	unsigned char rx[4] = {0};
	unsigned long rx_len = 4;

	if (m_acr30u.contains(a_lun)) {
		ifddbg() << DBG_INFO  << "LUN" << a_lun << "already used!";
		// this is good
		return true;
	}

	acr30u *dev = new acr30u(a_lun);
	if (! dev) {
		ifddbg() << DBG_INFO  << "LUN" << a_lun << "ACR30U object not created!";
		return false;
	}

	if (! dev->initialize()) {
		ifddbg() << DBG_INFO  << "LUN" << a_lun << "failed to initialize ACR30U object!";
		return false;
	}

	m_acr30u[a_lun] = dev;
	ifddbg() << DBG_INFO  << "LUN" << a_lun << "created ACR30U object";

	// put IFD and card into known state
	is_ICC_present(a_lun);
	power_ICC(a_lun, ACR30U_IFD_UNPOWER, rx, &rx_len);

	return true;
}

bool ifd::destroy_channel(unsigned long a_lun) {
	ifddbg() << DBG_INFO << "LUN" << a_lun;

	if (! m_acr30u.contains(a_lun)) {
		ifddbg() << DBG_INFO  << "LUN" << a_lun << "not used!";
		return false;
	}

	acr30u *dev = m_acr30u[a_lun];
	if (dev) {
		delete dev;
		m_acr30u.remove(a_lun);
		ifddbg() << DBG_INFO  << "LUN" << a_lun << "destroyed ACR30U object";
	}

	return true;
}

bool ifd::set_capabilities(unsigned long a_lun,
		unsigned long a_tag,
		unsigned long a_length,
		unsigned char *a_value) {
	ifddbg() << DBG_INFO;

	_UNUSED(a_lun);
	_UNUSED(a_tag);
	_UNUSED(a_length);
	_UNUSED(a_value);

	return false;
}

bool ifd::set_protocol_parameters(unsigned long a_lun,
		unsigned long a_protocol,
		unsigned char a_flags,
		unsigned char a_PTS1,
		unsigned char a_PTS2,
		unsigned char a_PTS3) {
	ifddbg() << DBG_INFO;

	_UNUSED(a_lun);
	_UNUSED(a_protocol);
	_UNUSED(a_flags);
	_UNUSED(a_PTS1);
	_UNUSED(a_PTS2);
	_UNUSED(a_PTS3);

	return false;
}

bool ifd::power_ICC(unsigned long a_lun,
		unsigned long a_action,
		unsigned char *a_atr,
		unsigned long *a_atr_length) {
	ifddbg() << DBG_INFO;

	bool rv = false;

	if (! m_acr30u.contains(a_lun)) {
		ifddbg() << DBG_INFO  << "LUN" << a_lun << "not used!";
		return false;
	}

	// XXX: In order to successfully power up the memory card
	//      the card type must be set/selected *prior* issuing
	//      power on or reset command.

	if (a_action == ACR30U_IFD_POWER) {
		rv = m_acr30u[a_lun]->power_card(a_atr, a_atr_length);
	} else if (a_action == ACR30U_IFD_UNPOWER) {
		rv = m_acr30u[a_lun]->unpower_card();
	} else if (a_action == ACR30U_IFD_RESET) {
		rv = m_acr30u[a_lun]->reset_card(a_atr, a_atr_length);
	}

	return rv;
}

bool ifd::transmit_to_ICC(unsigned long a_lun,
		unsigned long a_proto,
		unsigned char *a_tx_buffer,
		unsigned long a_tx_length,
		unsigned char *a_rx_buffer,
		unsigned long *a_rx_length) {
	ifddbg() << DBG_INFO;

	bool rv = false;

	if (! m_acr30u.contains(a_lun)) {
		ifddbg() << DBG_INFO  << "LUN" << a_lun << "not used!";
		return false;
	}

	// XXX: handle other protocols - T0 and T1
	// PCSC define for RAW access (4)
	if (a_proto == 4) {
		rv = m_acr30u[a_lun]->raw_exchange(a_tx_buffer, a_tx_length,
				a_rx_buffer, a_rx_length);
	} else {
		ifddbg() << DBG_INFO  << "LUN" << a_lun
				<< "unsupported protocol" << a_proto;
		return false;
	}

	return rv;
}

bool ifd::control(unsigned long a_lun,
		unsigned long a_control_code,
		unsigned char *a_tx_buffer,
		unsigned long a_tx_length,
		unsigned char *a_rx_buffer,
		unsigned long *a_rx_length) {
	ifddbg() << DBG_INFO;

	_UNUSED(a_tx_length);
	bool rv = false;

	if (! m_acr30u.contains(a_lun)) {
		ifddbg() << DBG_INFO  << "LUN" << a_lun << "not used!";
		return false;
	}

	ifddbg() << DBG_INFO  << "LUN" << a_lun
			<< "control code" << a_control_code;

	// error condition
	*a_rx_length = 0;

	switch (a_control_code) {
	case ACR30U_IFD_SETCARD:
		rv = m_acr30u[a_lun]->select_card(a_tx_buffer[0]);
		if (rv) {
			a_rx_buffer[0] = 0x90;
			a_rx_buffer[1] = 0x00;
			*a_rx_length = 2;
		} else {
			a_rx_buffer[0] = 0x60;
			a_rx_buffer[1] = 0x01;
			*a_rx_length = 2;
		}
		break;

	case ACR30U_IFD_NOTIFY:
		rv = m_acr30u[a_lun]->set_notification(a_tx_buffer[0]);
		if (rv) {
			a_rx_buffer[0] = 0x90;
			a_rx_buffer[1] = 0x00;
			*a_rx_length = 2;
		} else {
			a_rx_buffer[0] = 0x60;
			a_rx_buffer[1] = 0x00;
			*a_rx_length = 2;
		}
		break;

	case ACR30U_IFD_RESET:
		rv = m_acr30u[a_lun]->reset_card(a_rx_buffer, a_rx_length);
		break;

	case ACR30U_IFD_UNPOWER:
		rv = m_acr30u[a_lun]->unpower_card();
		memset(a_rx_buffer, 0, 4);
		*a_rx_length = 0;
		break;

	default:
		ifddbg() << DBG_INFO  << "LUN" << a_lun
			<< "unsupported control code" << a_control_code;
		break;
	}

	return rv;
}

bool ifd::is_IFD_present(unsigned long a_lun) {
	ifddbg() << DBG_INFO  << "LUN" << a_lun;
	bool rv = false;

	if (! m_acr30u.contains(a_lun)) {
		ifddbg() << DBG_INFO  << "LUN" << a_lun << "not used!";
		return false;
	}

	rv = m_acr30u[a_lun]->is_attached();
	if (! rv) {
		ifddbg() << DBG_INFO  << "LUN" << a_lun << "not attached!";
		// destroying the channel
		destroy_channel(a_lun);
	}

	return rv;
}

bool ifd::is_ICC_present(unsigned long a_lun) {
	ifddbg() << DBG_INFO  << "LUN" << a_lun;
	bool rv = false;

	if (! m_acr30u.contains(a_lun)) {
		ifddbg() << DBG_INFO  << "LUN" << a_lun << "not used!";
		return false;
	}

	rv = m_acr30u[a_lun]->is_card_present();

	return rv;
}

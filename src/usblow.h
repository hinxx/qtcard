/*
 * usblow.h
 *
 *  Created on: Nov 21, 2012
 *      Author: hinkocevar@gmail.com
 *     Project: QtCard
 *     Copying: See LICENSE
 *    Web page: https://bitbucket.org/hinxx/qtcard
 */

#ifndef USBLOW_H_
#define USBLOW_H_

#include <QtDebug>
#include <QObject>
#include <QStringList>

#include <usb.h>

#define _UNUSED(arg)	(void)arg;
#define DBG_INFO		__FILE__ << __func__ << __LINE__ << ": "

class usblow {
public:
	usblow(unsigned short a_vend_id, unsigned short a_prod_id,
			unsigned long a_ep_in, unsigned long a_ep_out);
	~usblow();

	bool open_dev();
	bool close_dev();
	bool write_dev(unsigned long length, unsigned char *buffer);
	bool read_dev(unsigned long *length, unsigned char *buffer);

private:
	usb_dev_handle *m_usbdev;
	unsigned short m_vend_id;
	unsigned short m_prod_id;
	unsigned long m_ep_in;
	unsigned long m_ep_out;
	unsigned long m_config;
	unsigned long m_intf;
};


#endif /* USBLOW_H_ */

/*
 * acr30u.cpp
 *
 *  Created on: Nov 21, 2012
 *      Author: hinkocevar@gmail.com
 *     Project: QtCard
 *     Copying: See LICENSE
 *    Web page: https://bitbucket.org/hinxx/qtcard
 */

#include <QStringList>

#include "usblow.h"
#include "acr30u.h"

// ACR30U USB reader IDs
#define USB_VEND_ID		0x072F
#define USB_PROD_ID		0x0001

// USB device endpoints
// Data is read on single bulk input channel
#define USB_EP_IN		0x81
// Data is always written to control channel
#define USB_EP_OUT		0x00

// USB device configuration and interface id
#define USB_CONFIG		1
#define USB_INTF		0

#ifdef ACR30U_DEBUG
inline QDebug acrdbg() { return QDebug(QtDebugMsg); }
#else
inline QNoDebug acrdbg() { return QNoDebug(); }
#endif

acr30u::acr30u(unsigned long a_lun) {
	acrdbg() << DBG_INFO;

	m_lun = a_lun;

	// initialize USB layer
	m_usb = new usblow(USB_VEND_ID, USB_PROD_ID, USB_EP_IN, USB_EP_OUT);
}

acr30u::~acr30u(void) {
	acrdbg() << DBG_INFO;

	deinitialize();
}

bool acr30u::initialize(void) {
	acrdbg() << DBG_INFO;

	// try to find and open our USB device
	return m_usb->open_dev();
}

void acr30u::deinitialize(void) {
	acrdbg() << DBG_INFO;

	delete m_usb;
}

bool acr30u::is_attached(void) {
	acrdbg() << DBG_INFO;

	bool rv = false;
	unsigned char status[ACR30U_MAX_BUFFER_SIZE];

	rv = get_stats(status);

	if (rv) {
		acrdbg() << DBG_INFO << "ACR30U present";
		return true;
	}

	acrdbg() << DBG_INFO << "ACR30U absent";
	return false;
}

bool acr30u::is_card_present(void) {
	acrdbg() << DBG_INFO;

	bool rv = false;
	unsigned char status[ACR30U_MAX_BUFFER_SIZE];

	rv = get_stats(status);

	if (rv) {
		if (status[15] == 0x01 || status[15] == 0x03) {
			acrdbg() << DBG_INFO << "card present";
			// card is inserted
			return true;
		} else {
			acrdbg() << DBG_INFO << "card absent";
			return false;
		}
	}

	acrdbg() << DBG_INFO << "error";
	return false;
}

bool acr30u::power_card(unsigned char *a_atr, unsigned long *a_atr_length) {
	acrdbg() << DBG_INFO;

	return reset_card(a_atr, a_atr_length);
}

bool acr30u::reset_card(unsigned char *a_atr, unsigned long *a_atr_length) {
	acrdbg() << DBG_INFO;

	bool rv = false;
	unsigned char reset[5];

	*a_atr_length = 0;

	reset[0] = 0x01;
	reset[1] = 0x80;
	reset[2] = 0x00;
	reset[3] = checksum(reset, 3);

	rv = transmit(reset, 4, a_atr, a_atr_length);

	// Turn off card status notification
	//set_notification(ACR30U_NOTIFY_FALSE);

	return rv;
}

bool acr30u::unpower_card(void) {
	acrdbg() << DBG_INFO;

	bool rv = false;
	unsigned long len;
	unsigned char powerdown[5];
	unsigned char response[ACR30U_MAX_BUFFER_SIZE];

	len = 0;

	powerdown[0] = 0x01;
	powerdown[1] = 0x81;
	powerdown[2] = 0x00;
	powerdown[3] = checksum(powerdown, 3);

	rv = transmit(powerdown, 4, response, &len);

	return rv;
}

bool acr30u::get_stats(unsigned char *a_status) {
	acrdbg() << DBG_INFO;

	bool rv = false;
	unsigned long len = 0;
	unsigned char status[5];
	unsigned char response[ACR30U_MAX_BUFFER_SIZE];

	status[0] = 0x01;
	status[1] = 0x01;
	status[2] = 0x00;
	status[3] = checksum(status, 3);

	rv = transmit(status, 4, response, &len);

	memcpy(a_status, response, len);

	return rv;
}

bool acr30u::select_card(unsigned char a_card_type) {
	acrdbg() << DBG_INFO;

	bool rv = false;
	unsigned long len = 0;
	unsigned char type[5];
	unsigned char response[ACR30U_MAX_BUFFER_SIZE];

	type[0] = 0x01;
	type[1] = 0x02;
	type[2] = 0x01;
	type[3] = a_card_type;
	type[4] = checksum(type, 4);

	rv = transmit(type, 5, response, &len);

	return rv;
}

bool acr30u::set_notification(unsigned char a_state) {
	acrdbg() << DBG_INFO;

	bool rv = false;
	unsigned char set[5];
	unsigned char rx[ACR30U_MAX_BUFFER_SIZE];
	unsigned long rx_len;

	set[0] = 0x01;
	set[1] = 0x06;
	set[2] = 0x01;
	set[3] = a_state;
	set[4] = checksum(set, 4);

	rv = transmit(set, 5, rx, &rx_len);

	return rv;
}

bool acr30u::transmit(unsigned char *a_tx_buffer,
		unsigned long a_tx_length,
		unsigned char *a_rx_buffer,
		unsigned long *a_rx_length) {
	acrdbg() << DBG_INFO;

	bool rv = false;
	unsigned char ucAsciiCommand[ACR30U_MAX_BUFFER_SIZE * 2]; /* For byte splitting */
	unsigned long ulAsciiLength;
	unsigned char ucResponse[ACR30U_MAX_BUFFER_SIZE * 2]; /* For byte splitting */
	unsigned char ucResponseCmd[ACR30U_MAX_BUFFER_SIZE];
	unsigned long ulLength;
	unsigned long byteCounter, retFlag, i;

	convert2ascii(a_tx_buffer, a_tx_length, &ucAsciiCommand[1],
			&ulAsciiLength);

	ucAsciiCommand[0] = 0x02; /* Add Header Byte */
	ucAsciiCommand[ulAsciiLength + 1] = 0x03; /* Add Footer Byte */

	if (! m_usb->write_dev(ulAsciiLength + 2, ucAsciiCommand)) {
		acrdbg() << DBG_INFO  << "write_dev() failed!";
		return false;
	}

	byteCounter = 0;
	retFlag = 0;
	do {
		unsigned long length = 8;
		rv = m_usb->read_dev(&length, &ucResponse[byteCounter]);

		if (! rv) {
			acrdbg() << DBG_INFO  << "read_dev() failed!";
			return false;
		}

		// Read data until you find the footer ETX
		for (i = byteCounter; i < byteCounter + 8; i++) {
			if (ucResponse[i] == 0x03) {
				byteCounter += ((i - byteCounter) + 1);
				retFlag = 1;
				break;
			}
		}

		if (retFlag == 1) {
			break;
		}

		byteCounter += 8;
	} while (byteCounter < 512);

	if (byteCounter >= 512) {
		acrdbg() << DBG_INFO  << "read_dev() invalid read size!";
		return false;
	}
	acrdbg() << DBG_INFO  << "read_dev() read" << byteCounter << "bytes";

	ulLength = 0;
	convert2hex(&ucResponse[1], byteCounter - 1, ucResponseCmd, &ulLength);

	if (ucResponseCmd[3] == 0xFF) {
		*a_rx_length = ucResponseCmd[4] * 256 + ucResponseCmd[5];
		memcpy(a_rx_buffer, &ucResponseCmd[6], *a_rx_length);
	} else {
		*a_rx_length = ucResponseCmd[3];
		memcpy(a_rx_buffer, &ucResponseCmd[4], *a_rx_length);
	}
	
	// check for response
	if (ucResponseCmd[1] != 0x90) {
		acrdbg() << DBG_INFO  << "ERROR response code" << QString("%1").arg(ucResponseCmd[1], 0, 16) << QString("%1").arg(ucResponseCmd[2], 0, 16);
		return false;
	}

	return true;
}

bool acr30u::raw_exchange(unsigned char *a_tx_buffer,
		unsigned long a_tx_length,
		unsigned char *a_rx_buffer,
		unsigned long *a_rx_length) {
	acrdbg() << DBG_INFO;

	bool rv = false;
	unsigned char cla;
	unsigned char ins;
	unsigned char tx_buf[ACR30U_MAX_BUFFER_SIZE];
	unsigned long len;

	if (*a_rx_length > ACR30U_MAX_BUFFER_SIZE) {
		acrdbg() << DBG_INFO  << "invalid RX length, clipping to" << ACR30U_MAX_BUFFER_SIZE;
		*a_rx_length = ACR30U_MAX_BUFFER_SIZE;
	}

	memset(tx_buf, 0, ACR30U_MAX_BUFFER_SIZE);
	cla = a_tx_buffer[0];
	ins = a_tx_buffer[1];
	acrdbg() << DBG_INFO  << "class" << cla << "instruction" << ins;

	if (cla != ACR30U_IFD_RAW) {
		acrdbg() << DBG_INFO  << "invalid class" << cla;
		return false;
	}

	switch (ins) {
		case ACR30U_ICC_READ_BIN:
			if (a_tx_length < 7) {
				acrdbg() << DBG_INFO  << "invalid TX length" << a_tx_length;
				return false;
			}

			tx_buf[0] = 0x01;					// SOH byte
			tx_buf[1] = 0x90;					// INS read memory
			tx_buf[2] = 0x03;					// len of this PKT
			tx_buf[3] = a_tx_buffer[2];			// high address bytes
			tx_buf[4] = a_tx_buffer[3];			// low address byte
			tx_buf[5] = a_tx_buffer[6];			// len of data to read
			tx_buf[6] = checksum(tx_buf, 6);	// xored checksum
			len = 7;

			rv = transmit(tx_buf, len, a_rx_buffer, a_rx_length);
			break;

		case ACR30U_ICC_UPDATE_BIN:
			if (a_tx_length < (unsigned long)(a_tx_buffer[4] + 5)) {
				acrdbg() << DBG_INFO  << "invalid TX length" << a_tx_length;
				return false;
			}

			tx_buf[0] = 0x01;					// SOH byte
			tx_buf[1] = 0x91;					// INS write data memory
			tx_buf[2] = a_tx_buffer[4] + 2;		// len of this PKT
			tx_buf[3] = a_tx_buffer[2];			// high address for write
			tx_buf[4] = a_tx_buffer[3];			// low address for write

			// data
			memcpy(&tx_buf[5], &a_tx_buffer[5], a_tx_buffer[4]);
			len = 5 + tx_buf[2];
			tx_buf[len] = checksum(tx_buf, len);// xored checksum

			rv = transmit (tx_buf, len, a_rx_buffer, a_rx_length);

			if (rv) {
				a_rx_buffer[0] = 0x90;
				a_rx_buffer[1] = 0x00;
				*a_rx_length = 2;
			} else {
				a_rx_buffer[0] = 0x60;
				a_rx_buffer[1] = 0x00;
				*a_rx_length = 2;
			}
			break;

		case ACR30U_ICC_VERIFY:
			if (a_tx_length < 8) {
				acrdbg() << DBG_INFO  << "invalid TX length" << a_tx_length;
				return false;
			}

			tx_buf[0] = 0x01;					// SOH byte
			tx_buf[1] = 0x92;					// INS check security code
			tx_buf[2] = 0x03;					// len of this PKT
			tx_buf[3] = a_tx_buffer[5];			// PIN byte1
			tx_buf[4] = a_tx_buffer[6];			// PIN byte2
			tx_buf[5] = a_tx_buffer[7];			// PIN byte3
			tx_buf[6] = checksum(tx_buf, 6);	// xored checksum
			len = 7;

			rv = transmit(tx_buf, len, a_rx_buffer, a_rx_length);
			break;

		case ACR30U_ICC_CHANGE:
			if (a_tx_length < 11) {
				acrdbg() << DBG_INFO  << "invalid TX length" << a_tx_length;
				return false;
			}

			// first verify card with old PIN
			tx_buf[0] = 0x01;					// SOH byte
			tx_buf[1] = 0x92;					// INS check security code
			tx_buf[2] = 0x03;					// len of this PKT
			tx_buf[3] = a_tx_buffer[5];			// old PIN byte1
			tx_buf[4] = a_tx_buffer[6];			// old PIN byte2
			tx_buf[5] = a_tx_buffer[7];			// old PIN byte3
			tx_buf[6] = checksum(tx_buf, 6);	// xored checksum
			len = 7;

			*a_rx_length = 12345;
			rv = transmit(tx_buf, len, a_rx_buffer, a_rx_length);
			qDebug() << DBG_INFO  << "RX length" << *a_rx_length;
			qDebug() << DBG_INFO  << "RX0 " << *a_rx_buffer << "RX1" << *(a_rx_buffer+1);
			qDebug() << DBG_INFO  << "rv" << rv;
			if (rv) {
				// now change the PIN to new value
				memset(tx_buf, 0, ACR30U_MAX_BUFFER_SIZE);
				tx_buf[0] = 0x01;				// SOH byte
				tx_buf[1] = 0x93;				// INS change security code
				tx_buf[2] = 0x03;				// len of this PKT
				tx_buf[3] = a_tx_buffer[8];		// new PIN byte1
				tx_buf[4] = a_tx_buffer[9];		// new PIN byte2
				tx_buf[5] = a_tx_buffer[10];	// new PIN byte3
				tx_buf[6] = checksum(tx_buf, 6);// xored checksum
				len = 7;

				rv = transmit(tx_buf, len, a_rx_buffer, a_rx_length);
			}
			break;

		default:
			acrdbg() << DBG_INFO  << "unsupported instruction" << ins;
			break;
	}

	return rv;
}

/*
 * private members
 */
void acr30u::convert2ascii(unsigned char *a_bin_buffer,
		unsigned long a_bin_length,
		unsigned char *a_ascii_buffer,
		unsigned long *a_ascii_length) {
	acrdbg() << DBG_INFO;

	unsigned char high, low;
	unsigned long p = 0;

	for (unsigned long i = 0; i < a_bin_length; i++) {
		/* FIX :: PORTABILITY ISSUE */
		high = a_bin_buffer[i] >> 4;
		low = a_bin_buffer[i] - high * 0x10;

		a_ascii_buffer[p] = (high == 0) ? '0' : a_ascii_buffer[p];
		a_ascii_buffer[p] = (high == 1) ? '1' : a_ascii_buffer[p];
		a_ascii_buffer[p] = (high == 2) ? '2' : a_ascii_buffer[p];
		a_ascii_buffer[p] = (high == 3) ? '3' : a_ascii_buffer[p];
		a_ascii_buffer[p] = (high == 4) ? '4' : a_ascii_buffer[p];
		a_ascii_buffer[p] = (high == 5) ? '5' : a_ascii_buffer[p];
		a_ascii_buffer[p] = (high == 6) ? '6' : a_ascii_buffer[p];
		a_ascii_buffer[p] = (high == 7) ? '7' : a_ascii_buffer[p];
		a_ascii_buffer[p] = (high == 8) ? '8' : a_ascii_buffer[p];
		a_ascii_buffer[p] = (high == 9) ? '9' : a_ascii_buffer[p];
		a_ascii_buffer[p] = (high == 0xA) ? 'A' : a_ascii_buffer[p];
		a_ascii_buffer[p] = (high == 0xB) ? 'B' : a_ascii_buffer[p];
		a_ascii_buffer[p] = (high == 0xC) ? 'C' : a_ascii_buffer[p];
		a_ascii_buffer[p] = (high == 0xD) ? 'D' : a_ascii_buffer[p];
		a_ascii_buffer[p] = (high == 0xE) ? 'E' : a_ascii_buffer[p];
		a_ascii_buffer[p] = (high == 0xF) ? 'F' : a_ascii_buffer[p];

		a_ascii_buffer[p + 1] = (low == 0) ? '0' : a_ascii_buffer[p + 1];
		a_ascii_buffer[p + 1] = (low == 1) ? '1' : a_ascii_buffer[p + 1];
		a_ascii_buffer[p + 1] = (low == 2) ? '2' : a_ascii_buffer[p + 1];
		a_ascii_buffer[p + 1] = (low == 3) ? '3' : a_ascii_buffer[p + 1];
		a_ascii_buffer[p + 1] = (low == 4) ? '4' : a_ascii_buffer[p + 1];
		a_ascii_buffer[p + 1] = (low == 5) ? '5' : a_ascii_buffer[p + 1];
		a_ascii_buffer[p + 1] = (low == 6) ? '6' : a_ascii_buffer[p + 1];
		a_ascii_buffer[p + 1] = (low == 7) ? '7' : a_ascii_buffer[p + 1];
		a_ascii_buffer[p + 1] = (low == 8) ? '8' : a_ascii_buffer[p + 1];
		a_ascii_buffer[p + 1] = (low == 9) ? '9' : a_ascii_buffer[p + 1];
		a_ascii_buffer[p + 1] = (low == 0xA) ? 'A' : a_ascii_buffer[p + 1];
		a_ascii_buffer[p + 1] = (low == 0xB) ? 'B' : a_ascii_buffer[p + 1];
		a_ascii_buffer[p + 1] = (low == 0xC) ? 'C' : a_ascii_buffer[p + 1];
		a_ascii_buffer[p + 1] = (low == 0xD) ? 'D' : a_ascii_buffer[p + 1];
		a_ascii_buffer[p + 1] = (low == 0xE) ? 'E' : a_ascii_buffer[p + 1];
		a_ascii_buffer[p + 1] = (low == 0xF) ? 'F' : a_ascii_buffer[p + 1];

		p += 2;
	}

	*a_ascii_length = p;

	QStringList sl;
	for (unsigned long i = 0; i < *a_ascii_length; ++i) {
		sl.append(QString("%1").arg(a_ascii_buffer[i], 0, 16));
	}
	acrdbg() << DBG_INFO << "length: " << *a_ascii_length
			<< " data: " << sl.join(" ");
}

void acr30u::convert2hex (unsigned char *a_ascii_buffer,
		unsigned long a_ascii_length,
		unsigned char *a_bin_buffer,
		unsigned long *a_bin_length) {
	acrdbg() << DBG_INFO;

	unsigned long p = 0;

	for (unsigned long i = 0; i < a_ascii_length; i += 2) {

		a_bin_buffer[p] = (a_ascii_buffer[i] == '0') ? 0 * 0x10 : a_bin_buffer[p];
		a_bin_buffer[p] = (a_ascii_buffer[i] == '1') ? 1 * 0x10 : a_bin_buffer[p];
		a_bin_buffer[p] = (a_ascii_buffer[i] == '2') ? 2 * 0x10 : a_bin_buffer[p];
		a_bin_buffer[p] = (a_ascii_buffer[i] == '3') ? 3 * 0x10 : a_bin_buffer[p];
		a_bin_buffer[p] = (a_ascii_buffer[i] == '4') ? 4 * 0x10 : a_bin_buffer[p];
		a_bin_buffer[p] = (a_ascii_buffer[i] == '5') ? 5 * 0x10 : a_bin_buffer[p];
		a_bin_buffer[p] = (a_ascii_buffer[i] == '6') ? 6 * 0x10 : a_bin_buffer[p];
		a_bin_buffer[p] = (a_ascii_buffer[i] == '7') ? 7 * 0x10 : a_bin_buffer[p];
		a_bin_buffer[p] = (a_ascii_buffer[i] == '8') ? 8 * 0x10 : a_bin_buffer[p];
		a_bin_buffer[p] = (a_ascii_buffer[i] == '9') ? 9 * 0x10 : a_bin_buffer[p];
		a_bin_buffer[p] = (a_ascii_buffer[i] == 'A') ? 10 * 0x10 : a_bin_buffer[p];
		a_bin_buffer[p] = (a_ascii_buffer[i] == 'B') ? 11 * 0x10 : a_bin_buffer[p];
		a_bin_buffer[p] = (a_ascii_buffer[i] == 'C') ? 12 * 0x10 : a_bin_buffer[p];
		a_bin_buffer[p] = (a_ascii_buffer[i] == 'D') ? 13 * 0x10 : a_bin_buffer[p];
		a_bin_buffer[p] = (a_ascii_buffer[i] == 'E') ? 14 * 0x10 : a_bin_buffer[p];
		a_bin_buffer[p] = (a_ascii_buffer[i] == 'F') ? 15 * 0x10 : a_bin_buffer[p];

		a_bin_buffer[p] += (a_ascii_buffer[i + 1] == '0') ? 0 : 0;
		a_bin_buffer[p] += (a_ascii_buffer[i + 1] == '1') ? 1 : 0;
		a_bin_buffer[p] += (a_ascii_buffer[i + 1] == '2') ? 2 : 0;
		a_bin_buffer[p] += (a_ascii_buffer[i + 1] == '3') ? 3 : 0;
		a_bin_buffer[p] += (a_ascii_buffer[i + 1] == '4') ? 4 : 0;
		a_bin_buffer[p] += (a_ascii_buffer[i + 1] == '5') ? 5 : 0;
		a_bin_buffer[p] += (a_ascii_buffer[i + 1] == '6') ? 6 : 0;
		a_bin_buffer[p] += (a_ascii_buffer[i + 1] == '7') ? 7 : 0;
		a_bin_buffer[p] += (a_ascii_buffer[i + 1] == '8') ? 8 : 0;
		a_bin_buffer[p] += (a_ascii_buffer[i + 1] == '9') ? 9 : 0;
		a_bin_buffer[p] += (a_ascii_buffer[i + 1] == 'A') ? 10 : 0;
		a_bin_buffer[p] += (a_ascii_buffer[i + 1] == 'B') ? 11 : 0;
		a_bin_buffer[p] += (a_ascii_buffer[i + 1] == 'C') ? 12 : 0;
		a_bin_buffer[p] += (a_ascii_buffer[i + 1] == 'D') ? 13 : 0;
		a_bin_buffer[p] += (a_ascii_buffer[i + 1] == 'E') ? 14 : 0;
		a_bin_buffer[p] += (a_ascii_buffer[i + 1] == 'F') ? 15 : 0;

		p += 1;
	}

	*a_bin_length = p;

	QStringList sl;
	for (unsigned long i = 0; i < *a_bin_length; ++i) {
		sl.append(QString("%1").arg(a_bin_buffer[i], 0, 16));
	}
	acrdbg() << DBG_INFO << "length: " << *a_bin_length
			<< " data: " << sl.join(" ");
}

unsigned char acr30u::checksum(unsigned char *a_buffer, unsigned long a_length) {
	acrdbg() << DBG_INFO;

	unsigned char chk = 0;

	for (unsigned long i = 0; i < a_length; i++) {
		chk ^= a_buffer[i];
	}

	return chk;
}

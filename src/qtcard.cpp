/*
 * qtcard.cpp
 *
 *  Created on: Nov 21, 2012
 *      Author: hinkocevar@gmail.com
 *     Project: QtCard
 *     Copying: See LICENSE
 *    Web page: https://bitbucket.org/hinxx/qtcard
 */


#include "qtcard.h"

#define CARD_MAGIC0		0xFFFFFFFF
#define CARD_MAGIC1		6969
#define CARD_MAGIC2		6970

#define CARD_STATUS_UNKNOWN		0
#define CARD_STATUS_ABSENT		1
#define CARD_STATUS_PRESENT		2

qtcard::qtcard(Qt::WFlags f)
	: QMainWindow(NULL, f)
{

	QCoreApplication::setOrganizationName("Hinxx");
	QCoreApplication::setOrganizationDomain("hinxx.org");
	QCoreApplication::setApplicationName("qtcard");

	// setup UI
	setupUi(this);

	// setup statusbar
    QString ver = QString("v0.3");
	m_statusbar_datetime = new QLabel();
    QLabel *author = new QLabel("QtCard "+ver+", by Hinko Kocevar (hinkocevar@gmail.com)");
	// far left
	statusbar->addWidget(author);
	// far right
	statusbar->addPermanentWidget(m_statusbar_datetime);

	m_card_icon = false;

	QPalette p = m_log->palette();
	p.setColor(QPalette::Base, QColor(0, 0, 0));
	m_log->setPalette(p);

    log_info("Welcome to qtcard "+ver+"!<br>");
	log_info("initializing ..");

    // obtain (platform specific) application's data/settings directory
    QSettings ini(QSettings::IniFormat, QSettings::UserScope,
    		QCoreApplication::organizationName(),
    		QCoreApplication::applicationName());
	restoreGeometry(ini.value("window/geometry").toByteArray());
	restoreState(ini.value("window/state").toByteArray());
	qDebug() << DBG_INFO << "Ui settings restored ..";

	bool ok;
//    m_pin1 = ini.value("settings/pin1", "FF").toString().toInt(&ok, 16);
//    m_pin2 = ini.value("settings/pin2", "FF").toString().toInt(&ok, 16);
//    m_pin3 = ini.value("settings/pin3", "FF").toString().toInt(&ok, 16);
    m_pin1 = ini.value("settings/pin1", "C0").toString().toInt(&ok, 16);
    m_pin2 = ini.value("settings/pin2", "DE").toString().toInt(&ok, 16);
    m_pin3 = ini.value("settings/pin3", "A5").toString().toInt(&ok, 16);

	connect(m_action_about, SIGNAL(triggered()),
			this, SLOT(about_slot()));
	connect(m_action_exit, SIGNAL(triggered()),
			this, SLOT(exit_slot()));
//	connect(m_action_settings, SIGNAL(triggered()),
//			this, SLOT(settings_slot()));
	connect(m_ui_updatebtn, SIGNAL(released()),
			this, SLOT(card_update_slot()));
	connect(m_ui_cid_change, SIGNAL(stateChanged(int)),
			this, SLOT(card_cid_update_slot(int)));

	m_ui_updatebtn->setDisabled(true);

    // find QSLite driver
//    m_db = QSqlDatabase::addDatabase("QSQLITE");

    // store database file into applications ini folder
//    QString config_dir = QFileInfo(ini.fileName()).absolutePath();
//    QString path(config_dir);
//    path.append(QDir::separator()).append("qtcard.db.sqlite3");
//    path = QDir::toNativeSeparators(path);
//    m_db.setDatabaseName(path);
//	log_info(QString("DB save path set to %1").arg(path));

    // open database
//    m_db.open();
//    if (! db_check()) {
//    	m_db.close();
//    	m_db.removeDatabase("QSQLITE");
//    	log_error("failed to initialize DB subsystem");
//    } else {
//    	db_last_cid();
//    }
    log_info(QString("DB is not used!"));

	// prepare IFD
	m_card_status = CARD_STATUS_UNKNOWN;
	m_ifd = new ifd();
	m_heartbeat = 0;

	log_info("initialization done!");

	// schedule a sate & time and smart card presence detection after some time
	QTimer::singleShot(100, this, SLOT(datetime_update_slot()));
}

qtcard::~qtcard() {
	// power off the card
	card_power(false);

	// destroy the channel and acr30u object
	m_ifd->destroy_channel(0);
	delete m_ifd;

	// close the DB
//	m_db.close();
//	m_db.removeDatabase("QSQLITE");

    QSettings ini(QSettings::IniFormat, QSettings::UserScope,
    		QCoreApplication::organizationName(),
    		QCoreApplication::applicationName());
	ini.setValue("window/geometry", saveGeometry());
	ini.setValue("window/state", saveState());
}

void qtcard::log_info(QString a_str) {
	//get current date and time
	QDateTime dateTime = QDateTime::currentDateTime();
	QString dateTimeString = dateTime.toString();
	m_log->append(QString("<p style='margin-top:0px; "
			"margin-bottom:0px; margin-left:0px; margin-right:0px; "
			"-qt-block-indent:0; text-indent:0px;'>"
			"<span style='color:#ffffff;'>%1: %2</span></p>").arg(dateTimeString).arg(a_str));
}

void qtcard::log_error(QString a_str) {
	//get current date and time
	QDateTime dateTime = QDateTime::currentDateTime();
	QString dateTimeString = dateTime.toString();
	m_log->append(QString("<p style='margin-top:0px; "
			"margin-bottom:0px; margin-left:0px; margin-right:0px; "
			"-qt-block-indent:0; text-indent:0px;'>"
			"<span style='color:#ff0000;'>%1: %2</span></p>").arg(dateTimeString).arg(a_str));
}

void qtcard::log_warning(QString a_str) {
	//get current date and time
	QDateTime dateTime = QDateTime::currentDateTime();
	QString dateTimeString = dateTime.toString();
	m_log->append(QString("<p style='margin-top:0px; "
			"margin-bottom:0px; margin-left:0px; margin-right:0px; "
			"-qt-block-indent:0; text-indent:0px;'>"
			"<span style='color:#ffaa00;'>%1: %2</span></p>").arg(dateTimeString).arg(a_str));
}

void qtcard::about_slot() {
	QMessageBox msgBox;

	QIcon myIcon(":/img/SmartCardHelp24.png");
	msgBox.setWindowIcon(myIcon);

	QFile f(":/text/about.html");
	f.open(QIODevice::ReadOnly | QIODevice::Text);
	const QByteArray data = f.readAll();
	f.close();
	const QString html = QTextCodec::codecForHtml(data)->toUnicode(data);

	msgBox.setText(html);
	msgBox.exec();
}

bool qtcard::card_power(bool a_state) {
	bool rv = false;
	unsigned char rx[4] = {0};
	unsigned long rx_len = 4;

	if (a_state) {
		// power on the card
		rv = m_ifd->power_ICC(0, ACR30U_IFD_POWER, rx, &rx_len);
		if (! rv) {
			log_error("failed to power on the card");
		} else {
			log_info("card powered on");

			// 4. SLE4442 response to reset - ATR: 0xa2 0x13 0x10 0x91
			if (! (rx[0] == 0xa2 && rx[1] == 0x13 && rx[2] == 0x10 && rx[3] == 0x91)) {
				card_error("invalid ATR response from the card");
				return false;
			}
			log_info("card reset, ATR OK");
		}
	} else {
		// power off the card
		rv = m_ifd->power_ICC(0, ACR30U_IFD_UNPOWER, rx, &rx_len);
		if (! rv) {
			log_error("failed to power off the card");
		} else {
			log_info("card powered off");
		}
	}

	return rv;
}

void qtcard::card_error(QString a_str) {
	m_ui_personalized->setPixmap(QPixmap(":/img/smiley_frown.png"));
	m_ui_valid->setPixmap(QPixmap(":/img/smiley_frown.png"));
	m_ui_master->setPixmap(QPixmap(":/img/smiley_frown.png"));
	m_ui_new_value->setText(QString(""));
	m_ui_total->setText(QString(""));
	m_ui_total->setPixmap(QPixmap(":/img/smiley_frown.png"));
	m_ui_value->setText(QString(""));
	m_ui_value->setPixmap(QPixmap(":/img/smiley_frown.png"));
	m_ui_cid->setText(QString(""));
//	m_ui_name->setText(QString(""));
//	m_ui_surname->setText(QString(""));
//	m_ui_address->setText(QString(""));
	m_ui_new_value->setReadOnly(true);
	m_ui_updatebtn->setDisabled(true);
	m_history->clear();
	m_history->setColumnCount(0);
	m_history->setRowCount(0);
//	m_ui_name->setReadOnly(true);
//	m_ui_surname->setReadOnly(true);
//	m_ui_address->setReadOnly(true);
	m_ui_cid->setReadOnly(true);
	m_ui_cid_change->setDisabled(true);
	m_ui_cid_change->setChecked(false);

	if (a_str.size()) {
		log_error(a_str);
	}
}

void qtcard::exit_slot() {
	qDebug() << DBG_INFO;
    log_info("Quitting..");

}

//void qtcard::settings_slot() {
//	qDebug() << DBG_INFO;

//	Ui::settings_dialog *sdialog = new Ui::settings_dialog();
//	QDialog *dialog = new QDialog(this);
//	sdialog->setupUi(dialog);

//    QSettings ini(QSettings::IniFormat, QSettings::UserScope,
//    		QCoreApplication::organizationName(),
//    		QCoreApplication::applicationName());
//    QString pin1 = ini.value("settings/pin1", "FF").toString().toUpper();
//    QString pin2 = ini.value("settings/pin2", "FF").toString().toUpper();
//    QString pin3 = ini.value("settings/pin3", "FF").toString().toUpper();

//    sdialog->m_pin1->setText(pin1);
//	sdialog->m_pin2->setText(pin2);
//	sdialog->m_pin3->setText(pin3);
//	int ret = dialog->exec();

//	if (ret == QDialog::Accepted) {
//		ini.setValue("settings/pin1", sdialog->m_pin1->text().toUpper());
//		ini.setValue("settings/pin2", sdialog->m_pin2->text().toUpper());
//		ini.setValue("settings/pin3", sdialog->m_pin3->text().toUpper());

//		bool ok;
//		m_pin1 = sdialog->m_pin1->text().toInt(&ok, 16);
//		m_pin2 = sdialog->m_pin2->text().toInt(&ok, 16);
//		m_pin3 = sdialog->m_pin3->text().toInt(&ok, 16);

//		qDebug() << DBG_INFO << "new PIN"
//				<< QString("%1").sprintf("%02lX", m_pin1)
//				<< " " << QString("%1").sprintf("%02lX", m_pin2)
//				<< " " << QString("%1").sprintf("%02lX", m_pin3);
//	} else {
//		qDebug() << DBG_INFO << "PIN not changed"
//				<< QString("%1").sprintf("%02lX", m_pin1)
//				<< " " << QString("%1").sprintf("%02lX", m_pin2)
//				<< " " << QString("%1").sprintf("%02lX", m_pin3);
//	}

//}

void qtcard::datetime_update_slot() {
	//qDebug() << DBG_INFO;

	//get current date and time
	QDateTime dateTime = QDateTime::currentDateTime();
	QString dateTimeString = dateTime.toString("yyyy-MM-dd hh:mm:ss");
	m_statusbar_datetime->setText(dateTimeString);

	m_mutex.lock();

	// see if IFD is attached
	if (m_ifd->is_IFD_present(0)) {
		// schedule card detection
		QTimer::singleShot(30, this, SLOT(card_detect_slot()));
	} else {
		// don't spam the log too much if IFD is not present (2 sec. rate)
		if ((m_heartbeat % 4) == 0) {
			log_warning("USB subsystem not yet initialized..");

			// try to initialize ACR30U reader access
			if (! m_ifd->create_channel(0, "ACR30U")) {
				log_error("failed to initialize USB subsystem");
				m_ui_card_presence->setPixmap(QPixmap(":/img/SmartCardError48.png"));
				// flag that card is absent
				m_card_status = CARD_STATUS_ABSENT;
			} else {
				log_info("USB subsystem initialized!");
				m_ui_card_presence->setPixmap(QPixmap(":/img/SmartCardSearch48.png"));
			}
		}
	}
	m_mutex.unlock();

	m_heartbeat++;

	// reschedule ourselves
	QTimer::singleShot(500, this, SLOT(datetime_update_slot()));
}

void qtcard::card_read_slot() {
	bool rv = false;
	unsigned char tx[ACR30U_MAX_BUFFER_SIZE] = {0};
	unsigned long tx_len;
	unsigned char rx[ACR30U_MAX_BUFFER_SIZE] = {0};
	unsigned long rx_len;

	m_mutex.lock();

//	QPalette p = m_ui_name->palette();
//	p.setColor(QPalette::Base, QColor(0xFF, 0xFF, 0xFF));
//	m_ui_name->setPalette(p);
//	p = m_ui_surname->palette();
//	p.setColor(QPalette::Base, QColor(0xFF, 0xFF, 0xFF));
//	m_ui_surname->setPalette(p);
//	p = m_ui_address->palette();
//	p.setColor(QPalette::Base, QColor(0xFF, 0xFF, 0xFF));
//	m_ui_address->setPalette(p);

	// if card is already in the slot, un-powered it first
	rv = card_power(false);
	if (! rv) {
		m_mutex.unlock();
		return;
	}

	// set card type for our SLE4442
	tx[0] = ACR30U_CARDTYPE_44X2;
	tx_len = 1;
	rx_len = 2;
	rv = m_ifd->control(0, ACR30U_IFD_SETCARD, tx, tx_len, rx, &rx_len);
	if (! rv) {
		card_error("failed to set SLE4442 card type");
		m_mutex.unlock();
		return;
	}

	// power on the card
	rv = card_power(true);
	if (! rv) {
		m_mutex.unlock();
		return;
	}

	// read 16 bytes at address 0x40
	tx[0] = ACR30U_IFD_RAW;
	tx[1] = ACR30U_ICC_READ_BIN;
	tx[2] = 0;
	tx[3] = 0x40;	// read from user data start
	tx[4] = 0;
	tx[5] = 0;
	tx[6] = 16;		// read 4 32-bit integers of data
	tx_len = 7;
	memset(rx, 0, sizeof(rx));
	rx_len = 16 + 4;
	rv = m_ifd->transmit_to_ICC(0, 4, tx, tx_len, rx, &rx_len);
	if (! rv) {
		card_error("failed to read the card");
		m_mutex.unlock();
		return;
	}

	// get data as integers
	m_card_magic = (rx[3] << 24 | rx[2] << 16 | rx[1] << 8 | rx[0]);
	m_card_id = (rx[7] << 24 | rx[6] << 16 | rx[5] << 8 | rx[4]);
	m_card_total = (rx[11] << 24 | rx[10] << 16 | rx[9] << 8 | rx[8]);
	m_card_value = (rx[15] << 24 | rx[14] << 16 | rx[13] << 8 | rx[12]);
	log_info(QString("card #%1").arg(m_card_id));
	log_info(QString("card magic %1").arg(m_card_magic));
	log_info(QString("card total %1").arg(m_card_total));
	log_info(QString("card value %1").arg(m_card_value));

	// verify magic
	// NOTE: if card was not yet initialized with correct magic value, only
	//       0xFFFFFFFF is acceptable - new card
	if (m_card_magic == CARD_MAGIC0) {
		log_warning(QString("invalid card magic %1").arg(m_card_magic));

		m_ui_personalized->setPixmap(QPixmap(":/img/no.png"));
		m_ui_valid->setPixmap(QPixmap(":/img/yes.png"));
		m_card_total = 0;
		m_card_value = 0;
	} else {
		if (m_card_magic == CARD_MAGIC1 || m_card_magic == CARD_MAGIC2) {
			log_info(QString("valid card magic %1").arg(m_card_magic));

			m_ui_personalized->setPixmap(QPixmap(":/img/yes.png"));
			m_ui_valid->setPixmap(QPixmap(":/img/yes.png"));

		} else {
			log_warning(QString("invalid card magic %1").arg(m_card_magic));

			m_ui_personalized->setPixmap(QPixmap(":/img/no.png"));
			m_ui_valid->setPixmap(QPixmap(":/img/no.png"));
			m_card_total = 0;
			m_card_value = 0;
		}
	}

	// set ui status ready for use
	m_ui_new_value->setText(QString("2"));
	// ID might be 0xFFFFFFFF
	if (m_card_id > 10000) {
		// new ID is one larger than DB value
//		m_ui_cid->setText(QString("%1").arg(db_last_cid() + 1));
        m_ui_cid->setText(QString("%1").arg(9999));
	} else {
		m_ui_cid->setText(QString("%1").arg(m_card_id));
	}

	m_ui_total->setPixmap(QPixmap());
	m_ui_total->setText(QString("%1 EUR").arg(m_card_total));
	m_ui_value->setPixmap(QPixmap());
	m_ui_value->setText(QString("%1 EUR").arg(m_card_value));
	m_ui_new_value->setReadOnly(false);
	m_ui_updatebtn->setDisabled(false);
	m_card_uname = QString("");
	m_card_usurname = QString("");
	m_card_uaddress = QString("");
	// do not allow CID changes by default
	m_ui_cid->setReadOnly(true);
	// allow forced CID changing
	m_ui_cid_change->setDisabled(false);

	// get user info from DB
//	m_ui_name->setReadOnly(false);
//	m_ui_surname->setReadOnly(false);
//	m_ui_address->setReadOnly(false);

//	db_user();

//	db_history();

//	m_ui_name->setText(m_card_uname);
//	m_ui_surname->setText(m_card_usurname);
//	m_ui_address->setText(m_card_uaddress);

	// detect "master" card
//	if (m_card_id == 1 && m_card_uname == "master" &&
//		m_card_usurname == "master" && m_card_uaddress == "master") {
    if (m_card_id == 1) {
        log_info(QString("master card inserted #%1").arg(m_card_id));
		m_ui_master->setPixmap(QPixmap(":/img/yes.png"));
	} else {
		m_ui_master->setPixmap(QPixmap(":/img/no.png"));
		log_info(QString("user card inserted #%1").arg(m_card_id));
	}

	log_info(QString("card #%1 is ready for use").arg(m_card_id));
	m_mutex.unlock();
}

void qtcard::card_update_slot() {
	qDebug() << DBG_INFO;
	bool rv = false;
	unsigned char tx[ACR30U_MAX_BUFFER_SIZE] = {0};
	unsigned long tx_len;
	unsigned char rx[ACR30U_MAX_BUFFER_SIZE] = {0};
	unsigned long rx_len;

	m_mutex.lock();

	// remember old card value
	m_card_old_value = 0;

	// do not allow to verify the card if PIN is not set
	if (m_pin1 == 0xFF && m_pin2 == 0xFF && m_pin3 == 0xFF) {
		log_error("refusing to verify the card with default PIN");
		m_mutex.unlock();
		return;
	}

	if (m_card_magic == CARD_MAGIC0) {
		log_warning(QString("invalid card magic %1").arg(m_card_magic));

		// we need new user data
//		m_card_uname = m_ui_name->text();
//		m_card_usurname = m_ui_surname->text();
//		m_card_uaddress = m_ui_address->text();

		// change the default PIN to user defined PIN
		tx[0] = ACR30U_IFD_RAW;
		tx[1] = ACR30U_ICC_CHANGE;
		tx[2] = 0;
		tx[3] = 0;
		tx[4] = 0;
		tx[5] = (unsigned char)0xFF;	// default PIN byte 1
		tx[6] = (unsigned char)0xFF;	// default PIN byte 2
		tx[7] = (unsigned char)0xFF;	// default PIN byte 3
		tx[8] = (unsigned char)m_pin1;	// PIN byte 1
		tx[9] = (unsigned char)m_pin2;	// PIN byte 2
		tx[10] = (unsigned char)m_pin3;	// PIN byte 3
		tx_len = 11;
		memset(rx, 0, sizeof(rx));
		rx_len = 16 + 4;
		rv = m_ifd->transmit_to_ICC(0, 4, tx, tx_len, rx, &rx_len);
		if (! rv) {
			card_error("failed to set new PIN for the  card");
			m_mutex.unlock();
			return;
		}

		log_info("card PIN was updated");

	} else if (m_card_magic == CARD_MAGIC1 || m_card_magic == CARD_MAGIC2) {
		log_info(QString("valid card magic %1").arg(m_card_magic));

		// verify the PIN
		tx[0] = ACR30U_IFD_RAW;
		tx[1] = ACR30U_ICC_VERIFY;
		tx[2] = 0;
		tx[3] = 0;
		tx[4] = 0;
		tx[5] = (unsigned char)m_pin1;	// PIN byte 1
		tx[6] = (unsigned char)m_pin2;	// PIN byte 2
		tx[7] = (unsigned char)m_pin3;	// PIN byte 3
		tx_len = 8;
		memset(rx, 0, sizeof(rx));
		rx_len = 16 + 4;
		rv = m_ifd->transmit_to_ICC(0, 4, tx, tx_len, rx, &rx_len);
		if (! rv) {
			card_error("failed to verify the card");
			m_mutex.unlock();
			return;
		}

		m_ui_personalized->setPixmap(QPixmap(":/img/yes.png"));
		m_ui_valid->setPixmap(QPixmap(":/img/yes.png"));

		log_info("card verified");

		// add remaining value (currently on the card)
		m_card_old_value = m_card_value;
	} else {
		// refuse to handle invalid / unsupported card magic
		log_warning(QString("invalid card magic %1").arg(m_card_magic));
		card_error("invalid card magic");
		m_mutex.unlock();
		return;
	}

	// set new data (allows to change the user data on-the-fly in the db_update())
//	m_card_uname = m_ui_name->text();
//	m_card_usurname = m_ui_surname->text();
//	m_card_uaddress = m_ui_address->text();

	// sanity checks
	bool sane = true;
//	if (m_card_uname.size() == 0) {
//		log_warning("missing user name value");
//		QPalette p = m_ui_name->palette();
//		p.setColor(QPalette::Base, QColor(0xFF, 0xAA, 0x00));
//		m_ui_name->setPalette(p);
//		sane = false;
//	}
//	if (m_card_usurname.size() == 0) {
//		log_warning("missing user surname value");
//		QPalette p = m_ui_surname->palette();
//		p.setColor(QPalette::Base, QColor(0xFF, 0xAA, 0x00));
//		m_ui_surname->setPalette(p);
//		sane = false;
//	}
//	if (m_card_uaddress.size() == 0) {
//		log_warning("missing user address value");
//		QPalette p = m_ui_address->palette();
//		p.setColor(QPalette::Base, QColor(0xFF, 0xAA, 0x00));
//		m_ui_address->setPalette(p);
//		sane = false;
//	}

	if (! sane) {
		m_mutex.unlock();
		return;
	}

	// XXX: from this point onwards the member variables
	// reflecting the card state are being changed!

	// always use latest magic
	m_card_magic = CARD_MAGIC2;

	// allow resetting the card value to 0 (is UI value equals 0)
	bool ok;
	int newval = m_ui_new_value->text().toInt(&ok);
	if (ok) {
		// value is not empty string
		if (newval == 0) {
			m_card_total = 0;
		} else {
			m_card_total = m_card_old_value + newval;
		}
	} else {
		// leave the value as is!
		m_card_total = m_card_old_value;
	}
	m_card_value = m_card_total;

	// ID might be 0xFFFFFFFF
	if (m_card_id > 10000) {
		// new ID is one larger than DB value
//		m_card_id = db_last_cid() + 1;
        m_card_id = 9999;
	}

	// force users value for CID if desired
	if (m_ui_cid_change->checkState() == Qt::Checked) {
		m_card_id = m_ui_cid->text().toInt();
		m_card_total = newval;
		m_card_value = m_card_total;
		m_card_old_value = 0;
	}

	// force "master" card ID if user data matches "master" and CID is 1
//	if (m_card_id == 1 && m_card_uname == "master" &&
//			m_card_usurname == "master" && m_card_uaddress == "master") {
    if (m_card_id == 1) {
        // value is not used on "master" cards
		m_card_total = 0;
		m_card_value = 0;
		m_card_old_value = 0;

		log_info(QString("creating master card #%1").arg(m_card_id));
		m_ui_master->setPixmap(QPixmap(":/img/yes.png"));
	} else {
		m_ui_master->setPixmap(QPixmap(":/img/no.png"));
		log_info(QString("creating user card #%1").arg(m_card_id));
	}

	// write 16 bytes at address 0x40 (always set id and magic)
	tx[0] = ACR30U_IFD_RAW;
	tx[1] = ACR30U_ICC_UPDATE_BIN;
	tx[2] = 0;			// high address byte
	tx[3] = 0x40;		// low address byte
	tx[4] = 16;			// write 4 32-bit integers of data
	tx[5] = (unsigned char)((m_card_magic) & 0xFF);
	tx[6] = (unsigned char)((m_card_magic) >> 8 & 0xFF);
	tx[7] = (unsigned char)((m_card_magic) >> 16 & 0xFF);
	tx[8] = (unsigned char)((m_card_magic) >> 24 & 0xFF);
	tx[9] = (unsigned char)((m_card_id) & 0xFF);
	tx[10] = (unsigned char)((m_card_id) >> 8 & 0xFF);
	tx[11] = (unsigned char)((m_card_id) >> 16 & 0xFF);
	tx[12] = (unsigned char)((m_card_id) >> 24 & 0xFF);
	tx[13] = (unsigned char)((m_card_total) & 0xFF);
	tx[14] = (unsigned char)((m_card_total) >> 8 & 0xFF);
	tx[15] = (unsigned char)((m_card_total) >> 16 & 0xFF);
	tx[16] = (unsigned char)((m_card_total) >> 24 & 0xFF);
	tx[17] = (unsigned char)((m_card_value) & 0xFF);
	tx[18] = (unsigned char)((m_card_value) >> 8 & 0xFF);
	tx[19] = (unsigned char)((m_card_value) >> 16 & 0xFF);
	tx[20] = (unsigned char)((m_card_value) >> 24 & 0xFF);

	tx_len = 21;
	memset(rx, 0, sizeof(rx));
	rx_len = 16 + 4;
	rv = m_ifd->transmit_to_ICC(0, 4, tx, tx_len, rx, &rx_len);
	if (! rv) {
		card_error("failed to read the card");
		m_mutex.unlock();
		return;
	}

	// update DB records
//	if (! db_update()) {
//		card_error("failed to update card DB records");
//	}

	m_ui_personalized->setPixmap(QPixmap(":/img/refresh.png"));
	m_ui_valid->setPixmap(QPixmap(":/img/refresh.png"));
	m_ui_master->setPixmap(QPixmap(":/img/refresh.png"));

	// 7. schedule a card re-read after some time
	QTimer::singleShot(100, this, SLOT(card_read_slot()));

	log_info(QString("card #%1 was updated").arg(m_card_id));
	m_mutex.unlock();
}

void qtcard::card_cid_update_slot(int a_state) {
	//qDebug() << DBG_INFO;

	if (a_state == Qt::Checked) {
		m_ui_cid->setReadOnly(false);
	} else {
		m_ui_cid->setReadOnly(true);
	}
}

void qtcard::card_detect_slot() {
	//qDebug() << DBG_INFO;

	bool rv = false;

	m_mutex.lock();
	rv = m_ifd->is_ICC_present(0);
	if (rv) {
		m_ui_card_presence->setPixmap(QPixmap(":/img/SmartCardIn48.png"));

		// see if this is newly detected card
		if (m_card_status != CARD_STATUS_PRESENT) {
			m_ui_personalized->setPixmap(QPixmap(":/img/refresh.png"));
			m_ui_valid->setPixmap(QPixmap(":/img/refresh.png"));
			m_ui_master->setPixmap(QPixmap(":/img/refresh.png"));
			log_warning("card present");

			// flag that card is present
			m_card_status = CARD_STATUS_PRESENT;

			// schedule a card read after some time
			QTimer::singleShot(100, this, SLOT(card_read_slot()));
		}

	} else {
		m_card_icon = !m_card_icon;
		if (m_card_icon) {
			m_ui_card_presence->setPixmap(QPixmap(":/img/SmartCardOut48.png"));
		} else {
			m_ui_card_presence->setPixmap(QPixmap(":/img/SmartCardRefresh48.png"));
		}

		// see if this is newly removed card
		if (m_card_status != CARD_STATUS_ABSENT) {
			log_warning("card absent");

			// flag that card is absent
			m_card_status = CARD_STATUS_ABSENT;

			card_error(QString());

			// power off the card
			card_power(false);
		}
	}
	m_mutex.unlock();
}

//bool qtcard::db_check() {
//    qDebug() << DBG_INFO;

//	if (! m_db.isOpen()) {
//		qDebug() << DBG_INFO << "DB not opened:" << m_db.lastError();
//		return false;
//	}

//	{
//		// see if table cards exists
//		QSqlQuery query1(QString("SELECT name FROM sqlite_master "
//				"WHERE type='table' AND name='cards'"));
//		if (query1.next()) {
//			qDebug() << DBG_INFO << "cards table exists";
//		} else {
//			// create table cards
//			QSqlQuery query2(QString("CREATE TABLE cards "
//					"(id INTEGER PRIMARY KEY, magic NUMERIC, cid NUMERIC, "
//					"total NUMERIC, uname TEXT, usurname TEXT, uaddress TEXT, "
//					"created DATETIME);"));

//			// see if table cards exists
//			QSqlQuery query3(QString("SELECT name FROM sqlite_master "
//					"WHERE type='table' AND name='cards'"));
//			if (query3.next()) {
//				qDebug() << DBG_INFO << "cards table exists";
//			}
//		}
//	}

//	{
//		// see if table updates exists
//		QSqlQuery query1(QString("SELECT name FROM sqlite_master "
//				"WHERE type='table' AND name='updates'"));
//		if (query1.next()) {
//			qDebug() << DBG_INFO << "cards table exists";
//		} else {
//			// create table updates
//			QSqlQuery query2(QString("CREATE TABLE updates "
//					"(id INTEGER PRIMARY KEY, cid NUMERIC, old_value NUMERIC, "
//					"new_value NUMERIC, updated DATETIME)"));

//			// see if table updates exists
//			QSqlQuery query3(QString("SELECT name FROM sqlite_master "
//					"WHERE type='table' AND name='updates'"));
//			if (query3.next()) {
//				qDebug() << DBG_INFO << "cards table exists";
//			}
//		}
//	}

//	//qDebug() << DBG_INFO << "DB error" << m_db.lastError();
//	return true;
//}

//unsigned long qtcard::db_last_cid() {
//	qDebug() << DBG_INFO;

//	if (! m_db.isOpen()) {
//		qDebug() << DBG_INFO << "DB not opened:" << m_db.lastError();
//		return false;
//	}

//	unsigned long cid = 10000;

//	// get last card id
//	QSqlQuery query1(QString("SELECT cid FROM cards ORDER BY cid DESC"));
//	if (query1.next()) {
//		qDebug() << DBG_INFO << "cards table exists";
//		cid = query1.value(0).toInt();
//	}

//	qDebug() << DBG_INFO << "DB last CID" << cid;
//	return cid;
//}

//bool qtcard::db_update() {
//	qDebug() << DBG_INFO;

//	if (! m_db.isOpen()) {
//		qDebug() << DBG_INFO << "DB not opened:" << m_db.lastError();
//		return false;
//	}

//	QDateTime dateTime = QDateTime::currentDateTime();
//	QString dateTimeString = dateTime.toString("yyyy-MM-dd hh:mm:ss");

//	// see if card exists
//	QSqlQuery query1(QString("SELECT cid, uname, usurname, uaddress FROM cards WHERE cid = %1").arg(m_card_id));
//	if (query1.next()) {
//		qDebug() << DBG_INFO << "cards record exists for CID" << m_card_id;

//		// see if we need to update the user info
//		if ((m_card_uname != query1.value(1).toString())
//				|| (m_card_usurname != query1.value(2).toString())
//				|| (m_card_uaddress != query1.value(3).toString())) {

//			qDebug() << DBG_INFO << "updating cards record for CID" << m_card_id;

//			// update cards table for CID
//			QSqlQuery query2(QString("UPDATE cards "
//					"SET uname='%1', usurname='%2', uaddress='%3', created='%4' "
//					"WHERE cid = %5").arg(m_card_uname).arg(m_card_usurname).arg(m_card_uaddress).arg(dateTimeString).arg(m_card_id));
//		}

//	} else {
//		qDebug() << DBG_INFO << "creating cards record for CID" << m_card_id;

//		// insert into cards table
//		QSqlQuery query2(QString("INSERT INTO cards VALUES"
//				"(NULL, %1, %2, %3, '%4', '%5', '%6', '%7')").arg(m_card_magic).arg(m_card_id).arg(m_card_total).arg(m_card_uname).arg(m_card_usurname).arg(m_card_uaddress).arg(dateTimeString));
//	}

//	// insert into updates table
//	QSqlQuery query3(QString("INSERT INTO updates VALUES"
//			"(NULL, %1, %2, %3, '%4')").arg(m_card_id).arg(m_card_old_value).arg(m_card_total).arg(dateTimeString));

//	return true;
//}

//bool qtcard::db_user() {
//	qDebug() << DBG_INFO;

//	if (! m_db.isOpen()) {
//		qDebug() << DBG_INFO << "DB not opened:" << m_db.lastError();
//		return false;
//	}

//	// get user info
//	QSqlQuery query1(QString("SELECT uname, usurname, uaddress FROM cards WHERE cid = %1").arg(m_card_id));
//	if (query1.next()) {
//		qDebug() << DBG_INFO << "user exists for CID" << m_card_id;
//		m_card_uname = query1.value(0).toString();
//		m_card_usurname = query1.value(1).toString();
//		m_card_uaddress = query1.value(2).toString();

//		return true;
//	}

//	qDebug() << DBG_INFO << "user does not exists for CID" << m_card_id;
//	return false;
//}

//bool qtcard::db_history() {
//	qDebug() << DBG_INFO;

//	if (! m_db.isOpen()) {
//		qDebug() << DBG_INFO << "DB not opened:" << m_db.lastError();
//		return false;
//	}

//	// get user info
//	QSqlQuery query1(QString("SELECT old_value, new_value, updated, id "
//			"FROM updates WHERE cid = %1 ORDER BY id DESC").arg(m_card_id));
//	if (query1.lastError().isValid()) {
//		qDebug() << DBG_INFO << "Query error:" << query1.lastError();
//	}

//	m_history->clear();
//	QStringList sl;
//	sl.append("Updated on");
//	sl.append("Value");
//	m_history->setColumnCount(2);
//	m_history->setHorizontalHeaderLabels(sl);
//	int row = 0;
//	if (query1.isActive()) {
//		while (query1.next()) {
//			m_history->setRowCount(row + 1);
//			QTableWidgetItem *i = new QTableWidgetItem(query1.value(2).toString());
//			m_history->setItem(row, 0, i);
//			int val = query1.value(1).toInt() - query1.value(0).toInt();
//			i = new QTableWidgetItem(QString("%1").arg(val));
//			m_history->setItem(row, 1, i);
//			row++;
//		}
//	}

//	if(row == 0) {
//		m_history->setRowCount(row + 1);
//		QTableWidgetItem *i = new QTableWidgetItem(QString("No data"));
//		m_history->setItem(row, 0, i);
//		i = new QTableWidgetItem(QString("No data"));
//		m_history->setItem(row, 1, i);
//	}

//	return true;
//}

/*
 * main.cpp
 *
 *  Created on: Nov 21, 2012
 *      Author: hinkocevar@gmail.com
 *     Project: QtCard
 *     Copying: See LICENSE
 *    Web page: https://bitbucket.org/hinxx/qtcard
 */

#include <iostream>

#include <stdio.h>
#include <stdlib.h>

#include "qtcard_app.h"
#include "qtcard.h"

void myMessageOutput(QtMsgType type, const char *msg) {
	switch (type) {
	case QtDebugMsg:
//#ifndef QT_NO_DEBUG
		std::cerr << "Debug: " << msg << std::endl;
//#endif
		break;
	case QtWarningMsg:
		std::cerr << "Warning: " << msg << std::endl;
		break;
	case QtCriticalMsg:
		std::cerr << "Critical: " << msg << std::endl;
		break;
	case QtFatalMsg:
		std::cerr << "Fatal: " << msg << std::endl;
		abort();
		break;
	}
}

int main(int argc, char ** argv) {
	// take control of qDebug(), qWarning() and friends output
	qInstallMsgHandler(myMessageOutput);

	qtcard_app app(argc, argv);
	app.connect(&app, SIGNAL(lastWindowClosed()), &app, SLOT(quit()));

	qtcard win;
	app.connect(win.m_action_exit, SIGNAL(triggered()), &app, SLOT(quit()));

	win.show();

	return app.exec();
}

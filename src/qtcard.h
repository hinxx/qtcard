/*
 * qtcard.h
 *
 *  Created on: Nov 21, 2012
 *      Author: hinkocevar@gmail.com
 *     Project: QtCard
 *     Copying: See LICENSE
 *    Web page: https://bitbucket.org/hinxx/qtcard
 */

#ifndef QTCARD_H_
#define QTCARD_H_


#include <time.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <iostream>
#include <sstream>

// Qt headers
#include <QMainWindow>
#include <QtDebug>
#include <QObject>
#include <QVector>
#include <QMap>
#include <QDateTime>
#include <QTableWidget>
#include <QTableWidgetItem>
#include <QPushButton>
#include <QMessageBox>
#include <QInputDialog>
#include <QFileDialog>
#include <QMenu>
#include <QAction>
#include <QSettings>
#include <QTextCodec>
#include <QThread>
#include <QTimer>
#include <QMutex>
//#include <QSqlDatabase>
//#include <QSqlQuery>
//#include <QSqlError>
#include <QColor>
#include <QDate>
#include <QDialog>
#include <QBoxLayout>

// ACR30U library header
#include "ifd.h"

// UI header
#include "ui_mainui.h"
#include "ui_settingsui.h"

#define DBG_INFO		__FILE__ << __func__ << __LINE__ << ": "

class qtcard: public QMainWindow, public Ui::qtcard {
	Q_OBJECT

public:
	qtcard(Qt::WFlags f = 0);
	~qtcard();
	void log_info(QString a_str);
	void log_warning(QString a_str);
	void log_error(QString a_str);
	void card_error(QString a_str);
	bool card_power(bool a_state);
//	bool db_check();
//	unsigned long db_last_cid();
//	bool db_update();
//	bool db_user();
//	bool db_history();

public slots:
	void about_slot();
	void exit_slot();
	void card_read_slot();
	void card_update_slot();
	void card_cid_update_slot(int a_state);
	void card_detect_slot();
	void datetime_update_slot();
//	void settings_slot();

private:
	ifd *m_ifd;
	unsigned long m_card_status;

	unsigned long m_card_magic;
	unsigned long m_card_id;
	unsigned long m_card_total;
	unsigned long m_card_value;
	unsigned long m_card_old_value;
	QString m_card_uname;
	QString m_card_usurname;
	QString m_card_uaddress;
	bool m_card_icon;
	QMutex m_mutex;
//	QSqlDatabase m_db;
	unsigned long m_pin1;
	unsigned long m_pin2;
	unsigned long m_pin3;
	QLabel *m_statusbar_datetime;
	unsigned long long m_heartbeat;
};

#endif /* QTCARD_H_ */
